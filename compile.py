#! /usr/bin/env python3.8

import logging
from pathlib import Path

sourcedir = Path("data")
output = "output"

with open(output, 'wb') as file:
    pass

with open(output, 'ab') as o:
    cnt = 0
    readsize = 0
    while True:
        filename = f"{cnt}.txt"
        cnt += 1
        try:
            with open(sourcedir / filename, 'r') as f:
                for line in f:
                    linebytes = bytes.fromhex(line)
                    o.write(linebytes)
                    readsize += len(linebytes)
                    print(f"Size: {readsize / (1024 * 1024):7.4f}MB", end='\r', flush=True)
        except FileNotFoundError:
            break
        except Exception:
            logging.exception(f"error occurred: {filename}")
